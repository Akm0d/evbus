#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import evbus.scripts

if __name__ == "__main__":
    evbus.scripts.start()
