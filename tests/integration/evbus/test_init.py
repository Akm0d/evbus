import mock
import pytest
import tempfile


def test_cli(hub):
    with mock.patch("sys.argv", ["evbus"]):
        # Stop everything before it starts
        hub.evbus.RUN_FOREVER = False

        hub.pop.Loop.run_until_complete(hub.evbus.init.stop())

        # Run the cli, it should return immediately
        hub.evbus.init.cli()


@pytest.mark.asyncio
async def test_start(hub):
    hub.evbus.RUN_FOREVER = False
    await hub.evbus.init.start({})


@pytest.mark.asyncio
async def test_profiles(hub):
    channels = []
    data = {
        "pika": {
            # Host is None so that even if there is an active local rabbitmq server we don't connect
            "ingress_test_profile": {"host": None, "channels": channels},
            "ignored_profile": {"host": None, "channels": channels},
        }
    }
    with tempfile.NamedTemporaryFile(delete=True) as fh:
        key = hub.crypto.fernet.generate_key()
        enc = hub.crypto.fernet.encrypt(data, key)
        fh.write(enc)
        fh.flush()

        contexts = await hub.evbus.init.profiles(
            ["evbus"],
            acct_file=fh.name,
            acct_key=key,
            acct_profiles=["ingress_test_profile"],
        )

    assert "ingress_test_profile" in contexts["pika"]
    assert "ignored_profile" not in contexts["pika"]

    assert contexts["pika"]["ingress_test_profile"] == {
        "channels": channels,
        "connected": False,
        "connection": None,
    }
