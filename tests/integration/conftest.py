import asyncio
import mock
import pytest
import uuid

DEFAULT_PROFILE = "ingress_test_profile"


@pytest.fixture(scope="function")
def hub(hub):
    hub.pop.sub.add(dyne_name="evbus")

    with mock.patch("sys.argv", ["evbus"]):
        hub.pop.config.load(["evbus", "acct", "rend"], cli="evbus")

    yield hub


@pytest.mark.asyncio
@pytest.fixture(scope="function")
async def evbus(hub):
    """
    A bare minimum evbus with only local queues and no outside connections
    """
    task = asyncio.create_task(hub.evbus.init.start({}))

    await hub.evbus.init.join()

    yield

    await hub.evbus.init.stop()
    await task


@pytest.mark.asyncio
@pytest.fixture(scope="function")
async def rabbitmq_evbus(hub):
    """
    Start evbus with a rabbitmq connection
    """
    channel_name = f"yellow-bus-test-{uuid.uuid4()}"
    # TODO get these by setting hub.OPT.acct.acct_file and hub.OPT.acct.acct_key from the cli for CI
    # Credentials for connecting to a local rabbitmq server
    profiles = {
        "pika": {
            DEFAULT_PROFILE: {
                "host": "localhost",
                "port": 5672,
                "ingress_channels": [channel_name],
            }
        }
    }
    sub_profiles = await hub.acct.init.process(["evbus"], profiles)

    task = asyncio.create_task(hub.evbus.init.start(sub_profiles["evbus"]))

    await hub.evbus.init.join()

    ctx = sub_profiles.evbus.pika[DEFAULT_PROFILE]

    if not ctx.connected:
        pytest.skip("No local rabbitmq server")

    yield ctx, channel_name

    # Cleanup
    async with ctx.connection.channel() as channel:
        await channel.queue_delete(channel_name)

    await ctx.connection.close()

    # Stop evbus
    await hub.evbus.init.stop()
    await task


@pytest.fixture(scope="function")
def event(hub):
    return str(uuid.uuid4())
