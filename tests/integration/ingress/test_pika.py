import json
import pytest

TIMEOUT = 10


@pytest.mark.asyncio
async def test_publish(hub, rabbitmq_evbus, event):
    ctx, routing_key = rabbitmq_evbus

    async with await ctx.connection.channel() as channel:
        # First make sure that the queue exists
        q = await channel.declare_queue(routing_key, timeout=TIMEOUT, auto_delete=True)

        # Publish an event to the queue
        await hub.ingress.pika.publish(ctx, event)

        # Verify that the event made it onto the queue
        async with q.iterator() as queue_iter:
            async for message in queue_iter:
                async with message.process():
                    assert message.body.decode() == json.dumps(event)

                break


@pytest.mark.asyncio
async def test_full(hub, rabbitmq_evbus, event):
    """
    Test the path of an event through the bus
    The event fixture makes sure the event bus is running.
    Put an event directly on the event BUS so that it gets vomited onto all the ingress queues
    Verify that it made it all the way through to ingress
    """
    ctx, routing_key = rabbitmq_evbus

    async with ctx.connection.channel() as channel:
        # First make sure that the queue exists
        q = await channel.declare_queue(routing_key, timeout=TIMEOUT, auto_delete=True)

        # Put an event on the queue
        await hub.evbus.BUS.put(event)

        # Verify that the event made it onto the queue
        async with q.iterator() as queue_iter:
            async for message in queue_iter:
                async with message.process():
                    assert message.body.decode() == json.dumps(event)

                break
